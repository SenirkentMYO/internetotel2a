﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OtelProjesi
{
    public class Personeller
    {
        public int PersonelID { get; set; }
        public string PersonelAdi { get; set; }
        public string PersonelSoyAdi { get; set; }
        public string TcNO { get; set; }
        public DateTime DogumTarihi { get; set; }
        public string Gorevi { get; set; }
        public DateTime İseBaslangicTarihi { get; set; }
        public string Maasi { get; set; }
    }
}