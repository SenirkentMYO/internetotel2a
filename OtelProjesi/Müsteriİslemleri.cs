﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OtelEntity;

namespace OtelProjesi
{
    public class Müsteriİslemleri
    {
        public List<MüsteriEkleme> MüsteriListesi()
        {
            List<MüsteriEkleme> liste = new List<MüsteriEkleme>();
             using ( SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString()))
             {
                 baglanti.Open();
                 SqlCommand komut = new SqlCommand("Select * From MüsteriKayit", baglanti);
                 SqlDataReader dr = komut.ExecuteReader();
                 while (dr.Read())
                 {
                     MüsteriEkleme me = new MüsteriEkleme();
                     me.MüsteriAdi = dr["MüsteriAdi"].ToString();
                     me.MüsteriSoyAdi = dr["MüsteriSoyAdi"].ToString();
                     me.TcNo = dr["TcNo"].ToString();
                     liste.Add(me);


                 }
             }
             return liste;
        }
    }
}

      