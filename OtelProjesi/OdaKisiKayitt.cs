﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OtelProjesi
{
    public class OdaKisiKayitt
    {
        public int OdaKayitID { get; set; }
        public string Adi { get; set; }
        public string SoyAdi { get; set; }
        public string TcNO { get; set; }
        public DateTime OdaGirisTarihi { get; set; }
        public DateTime OdaCikisTarihi { get; set; }
        public int OdaNO { get; set; }
        public bool Cocuk { get; set; }
    }
}