﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OtelEntity;

namespace OtelProjesi
{
    public partial class MüsteriEkle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            cnn.Open();
            SqlCommand komut = new SqlCommand("Select * From MüsteriKayit", cnn);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<MüsteriEkleme> MüsteriListesi = new List<MüsteriEkleme>();
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            cnn.Open();
            {
                SqlCommand cmd = new SqlCommand("Select * From MüsteriKayit", cnn);
                SqlDataReader satirlar = cmd.ExecuteReader();
                while (satirlar.Read())
                {
                    MüsteriEkleme m = new MüsteriEkleme();
                    m.MüsteriAdi = satirlar["MüsteriAdi"].ToString();
                    m.MüsteriSoyAdi = satirlar["MüsteriSoyAdi"].ToString();
                    m.TcNo = satirlar["TcNo"].ToString();
                    m.DogumTarihi = (DateTime)satirlar["DogumTarihi"];
                    m.Adres = satirlar["Adres"].ToString();
                    m.Email = satirlar["Email"].ToString();
                    m.MüsteriKayitTarihi = (DateTime)satirlar["MüsteriKayitTarihi"];
                    m.TelNo = satirlar["TelNo"].ToString();

                    MüsteriListesi.Add(m);
                    cnn.Close();
                    cnn.Dispose();
                    Repeater1.DataSource = MüsteriListesi;
                    Repeater1.DataBind();


                }
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            List<MüsteriEkleme> MüsteriListesi = new List<MüsteriEkleme>();
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            cnn.Open();
            {
                SqlCommand cmd = new SqlCommand("Select * From MüsteriKayit", cnn);
                SqlDataReader satirlar = cmd.ExecuteReader();
                while (satirlar.Read())
                {
                    MüsteriEkleme m = new MüsteriEkleme();
                    m.MüsteriAdi = satirlar["MüsteriAdi"].ToString();
                    m.MüsteriSoyAdi = satirlar["MüsteriSoyAdi"].ToString();
                    m.DogumTarihi = (DateTime)satirlar["DogumTarihi"];
                    m.Email = satirlar["Email"].ToString();
                    m.MüsteriKayitTarihi = (DateTime)satirlar["MüsteriKayitTarihi"];


                    MüsteriListesi.Add(m);
                    cnn.Close();
                    cnn.Dispose();
                    DataList1.DataSource = MüsteriListesi;
                    DataList1.DataBind();



                }
            }
        }
    }
}