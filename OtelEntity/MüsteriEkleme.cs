﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OtelEntity
{
    public class MüsteriEkleme
    {
        public int MüsteriID { get; set; }
        public string MüsteriAdi { get; set; }
        public string MüsteriSoyAdi { get; set; }
        public string TcNo { get; set; }
        public DateTime DogumTarihi { get; set; }
        public string Adres { get; set; }
        public string Email { get; set; }
        public DateTime MüsteriKayitTarihi { get; set; }
        public string TelNo { get; set; }

    }
}