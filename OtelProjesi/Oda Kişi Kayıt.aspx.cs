﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OtelProjesi
{
    public partial class Oda_Kişi_Kayıt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<OdaKisiKayitt> Kayıtlar = new List<OdaKisiKayitt>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            {
                baglanti.Open();

                SqlCommand komut = new SqlCommand("Select * From OdaKisiKayit", baglanti);
                SqlDataReader satirlar = komut.ExecuteReader();
                while (satirlar.Read())
                {
                    OdaKisiKayitt okk = new OdaKisiKayitt();
                    okk.Adi = satirlar["Adi"].ToString();
                    okk.Cocuk = (bool)satirlar["Cocuk"];
                    okk.OdaCikisTarihi = (DateTime)satirlar["OdaCikisTarihi"];
                    okk.OdaGirisTarihi = (DateTime)satirlar["OdaGirisTarihi"];
                    okk.OdaNO = (int)satirlar["OdaNO"];
                    okk.SoyAdi = satirlar["SoyAdi"].ToString();
                    okk.TcNO = satirlar["TcNO"].ToString();

                    Kayıtlar.Add(okk);
                    baglanti.Close();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Oda_Kişi_Kayıt> OdaKisileri = new List<Oda_Kişi_Kayıt>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            {
                baglanti.Open();
                SqlCommand komut = new SqlCommand("Select * From OdaKisiKayit", baglanti);
                SqlDataReader satirlar = komut.ExecuteReader();
                while (satirlar.Read())
                {
                    OdaKisiKayitt odk = new OdaKisiKayitt();
                    odk.Adi = satirlar["Adi"].ToString();
                    odk.OdaCikisTarihi = (DateTime)satirlar["OdaCikisTarihi"];
                    odk.OdaGirisTarihi = (DateTime)satirlar["OdaGirisTarihi"];
                    odk.OdaNO = (int)satirlar["OdaNo"];
                    odk.SoyAdi = satirlar["SoyAdi"].ToString();
                    odk.TcNO = satirlar["TcNO"].ToString();

                    //OdaKisileri.Add(odk);
                    //  baglanti.Close();

                }
            }
        }
    }
}