﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OtelProjesi
{
    public class Kullanicilar
    {
        public int ID { get; set; }
        public string KullaniciAdi { get; set; }
        public string KullaniciSifre { get; set; }
    }
}