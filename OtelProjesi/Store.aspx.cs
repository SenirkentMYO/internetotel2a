﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OtelProjesi
{
    public partial class Store : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StoreP ki = new StoreP();
            Kullanicilar kul = ki.Giris(txtEmail.Text, txtSifre.Text);

            if (kul == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                Session["GirenUye"] = kul; Response.Redirect("MüsteriEkle.aspx");
            }
        }
    }
}